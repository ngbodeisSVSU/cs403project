package com.example.cs403finalproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_order_total.*

class OrderTotal : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_total)

        var totalPrice: String = intent.getStringExtra("totalPrice")
        var totalCount: String = intent.getStringExtra("totalCount")

        textViewTotalCount.setText(totalCount)
        textViewTotalPrice.setText(totalPrice)

        val btn_click_back = findViewById(R.id.btnMenu) as Button

        btn_click_back.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }


    }
}
