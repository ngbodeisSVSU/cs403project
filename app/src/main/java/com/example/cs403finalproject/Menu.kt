package com.example.cs403finalproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Menu : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)


        val recyclerView = findViewById(R.id.recyclerView) as RecyclerView

        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        val foodItems = ArrayList<FoodTypes>()

        foodItems.add(FoodTypes("Burger","Plain"))


        val adapter = recyclerAdapter(foodItems)
        recyclerView.adapter = adapter

        val btn_click_back = findViewById(R.id.btnBack) as Button
        val btn_click_AddToOrder= findViewById(R.id.btnOrderNow) as Button

        btn_click_back.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        btn_click_AddToOrder.setOnClickListener {
           //add what's in the checkboxes to the order
            val intent = Intent(this, OrderItems::class.java)
            startActivity(intent)
        }
    }
}
