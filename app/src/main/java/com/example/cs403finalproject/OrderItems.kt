package com.example.cs403finalproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import kotlinx.android.synthetic.main.activity_order_items.*
import java.text.DecimalFormat
import kotlin.math.roundToLong

class OrderItems : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_items)

        val plainburgerPrice = 7.99
        val cheeseburgerPrice = 8.99
        val baconburgerPrice = 9.99
        val baconcheeseburgerPrice = 10.99
        val caesarSaladPrice = 5.99
        val italianSaladPrice = 3.99
        val chefSaladPrice = 4.99
        val pastaSaladPrice = 6.99
        val hotdogPrice = 1.99
        val cheeseSticksPrice = 0.99
        val grilledCheesePrice = 4.99
        val tacoPrice = 2.99
        var burgerPrice = 0.00
        var saladPrice = 0.00
        var totalhotdogPrice = 0.00
        var totalcheeseSticksPrice = 0.00
        var totalgrilledCheesePrice = 0.00
        var totaltacoPrice = 0.00
        var totalPrice = 0.00
        var totalCount = 0

        val btn_click_menu = findViewById(R.id.btnMenu) as Button
        val btn_click_back = findViewById(R.id.btnBack) as Button
        val btn_click_AddToOrder= findViewById(R.id.btnOrderItems) as Button

        val spinnerBurger: Spinner = findViewById(R.id.spinnerBurger)
        val spinnerBurgerCount: Spinner = findViewById(R.id.spinnerBurgerCount)
        val spinnerSalad: Spinner = findViewById(R.id.spinnerSalad)
        val spinnerSaladCount: Spinner = findViewById(R.id.spinnerSaladCount)
        val spinnerHotDog: Spinner = findViewById(R.id.spinnerHotDog)
        val spinnerCheeseSticks: Spinner = findViewById(R.id.spinnerCheeseSticks)
        val spinnerGrilledCheese: Spinner = findViewById(R.id.spinnerGrilledCheese)
        val spinnerTaco: Spinner = findViewById(R.id.spinnerTaco)


            ArrayAdapter.createFromResource(
            this,
                R.array.Salads,
                android.R.layout.simple_spinner_item).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinnerSalad.adapter = adapter

            ArrayAdapter.createFromResource(
                this,
                R.array.Burgers,
                android.R.layout.simple_spinner_item).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinnerBurger.adapter = adapter}

            ArrayAdapter.createFromResource(
                this,
                R.array.ItemCount,
                android.R.layout.simple_spinner_item).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinnerBurgerCount.adapter = adapter}

            ArrayAdapter.createFromResource(
                this,
                R.array.ItemCount,
                android.R.layout.simple_spinner_item).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinnerSaladCount.adapter = adapter}

            ArrayAdapter.createFromResource(
                this,
                R.array.ItemCount,
                android.R.layout.simple_spinner_item).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinnerGrilledCheese.adapter = adapter}

            ArrayAdapter.createFromResource(
                this,
                R.array.ItemCount,
                android.R.layout.simple_spinner_item).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinnerTaco.adapter = adapter}

            ArrayAdapter.createFromResource(
                this,
                R.array.ItemCount,
                android.R.layout.simple_spinner_item).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinnerCheeseSticks.adapter = adapter}

            ArrayAdapter.createFromResource(
                this,
                R.array.ItemCount,
                android.R.layout.simple_spinner_item).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinnerHotDog.adapter = adapter}

            btn_click_menu.setOnClickListener {
                val intent = Intent(this, Menu::class.java)
                startActivity(intent)
            }

            btn_click_back.setOnClickListener {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }

            btn_click_AddToOrder.setOnClickListener {
            //add what's in the checkboxes to the order
                val intent = Intent(this, OrderTotal::class.java)

                var burgerCount = spinnerBurgerCount.selectedItem.toString()
                var saladCount = spinnerSaladCount.selectedItem.toString()
                var cheesestickCount = spinnerCheeseSticks.selectedItem.toString()
                var hotdogCount = spinnerHotDog.selectedItem.toString()
                var grilledcheeseCount = spinnerGrilledCheese.selectedItem.toString()
                var tacoCount = spinnerTaco.selectedItem.toString()

                if(checkBoxBurger.isChecked()){
                    var burgerType = spinnerBurger.selectedItem.toString()
                    totalCount =+ burgerCount.toInt()
                    if(burgerType == "Plain"){
                        burgerPrice = burgerCount.toDouble() * plainburgerPrice
                    }
                    if(burgerType == "Bacon"){
                        burgerPrice = burgerCount.toDouble() * baconburgerPrice
                    }
                    if(burgerType == "Cheese"){
                        burgerPrice = burgerCount.toDouble() * cheeseburgerPrice
                    }
                    if(burgerType == "Bacon Cheese"){
                        burgerPrice = burgerCount.toDouble() * baconcheeseburgerPrice
                    }
                }

                if(checkBoxSalad.isChecked()){
                    var saladType = spinnerSalad.selectedItem.toString()
                    totalCount += saladCount.toInt()
                    if(saladType == "Caesar"){
                        saladPrice = saladCount.toDouble() * caesarSaladPrice
                    }
                    if(saladType == "Italian"){
                        saladPrice = saladCount.toDouble() * italianSaladPrice
                    }
                    if(saladType == "Chef"){
                        saladPrice = saladCount.toDouble() * chefSaladPrice
                    }
                    if(saladType == "Pasta"){
                        saladPrice = saladCount.toDouble() * pastaSaladPrice
                    }
                }

                if(checkBoxCheeseSticks.isChecked()){
                    totalcheeseSticksPrice = cheesestickCount.toDouble() * cheeseSticksPrice
                    totalCount += cheesestickCount.toInt()

                }

                if(checkBoxHotDog.isChecked()){
                    totalhotdogPrice = hotdogCount.toDouble() * hotdogPrice
                    totalCount += hotdogCount.toInt()
                }

                if(checkBoxGrilledCheese.isChecked()){
                    totalgrilledCheesePrice = grilledcheeseCount.toDouble() * grilledCheesePrice
                    totalCount += grilledcheeseCount.toInt()
                }

                if(checkBoxTaco.isChecked()){
                    totaltacoPrice = tacoCount.toDouble() * tacoPrice
                    totalCount += tacoCount.toInt()
                }

                totalPrice = burgerPrice + saladPrice + totalcheeseSticksPrice + totalgrilledCheesePrice + totalhotdogPrice + totaltacoPrice



                intent.putExtra("totalPrice","%.2f".format(totalPrice))
                intent.putExtra("totalCount",totalCount.toString())
                startActivity(intent)
        }
        }
    }
}
