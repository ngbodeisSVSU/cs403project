package com.example.cs403finalproject

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class recyclerAdapter(val foodList: ArrayList<FoodTypes>) : RecyclerView.Adapter<recyclerAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.list_layout, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return foodList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val foodItem: FoodTypes = foodList[position]

        holder?.textViewFoodName.text = foodItem.name
        holder?.textViewFoodPrice.text = foodItem.price
    }


    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView){
        val textViewFoodName = itemView.findViewById(R.id.burgerfoodName) as TextView
        val textViewFoodPrice = itemView.findViewById(R.id.plainBurger) as TextView
    }
}